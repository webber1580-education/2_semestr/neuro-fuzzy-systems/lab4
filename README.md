# lab4

## Sources

1) Source paper - https://machinelearningmastery.com/how-to-perform-object-detection-with-yolov3-in-keras/
2) Source yolov3.weights - https://pjreddie.com/media/files/yolov3.weights
3) Source yolov3 explanation - https://towardsdatascience.com/yolo-v3-object-detection-53fb7d3bfe6b

## Run the app

1) Download yolov3.weights in the app root folder  
2) Run model.py script  
3) Specify path to target image in file yolo.py in line 187  
```
photo_filename = 'zebra.jpg'
```
4) Run yolo.py